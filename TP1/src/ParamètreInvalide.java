public class ParamètreInvalide extends Exception {

    public ParamètreInvalide(String params, String error) {
        super("Params: " + params + " | Erreurs: " + error);
    }
}
