public interface Creditable {

    void créditer(Couple<Devise, Float> couple) throws ParamètreInvalide;
}
