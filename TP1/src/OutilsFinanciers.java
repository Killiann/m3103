public class OutilsFinanciers {

    public static float tauxDeChange(Devise firt, Devise second) {
        switch (firt) {
            case EURO:
                switch (second) {
                    case EURO:
                        return 1F;
                    case DOLLAR:
                        return 1.31486F;
                }
                break;
            case DOLLAR:
                switch (second) {
                    case EURO:
                        return 0.760541F;
                    case DOLLAR:
                        return 1F;
                }
                break;
        }

        return 1F;
    }

    public static Couple<Devise, Float> convertir(Couple<Devise, Float> couple, Devise devise) {
        float taux = tauxDeChange(couple.getFirst(), devise);
        return new Couple<>(devise, couple.getSeconds()*taux);
    }
}
