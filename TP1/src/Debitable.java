public interface Debitable {

    void debiter(Couple<Devise, Float> couple) throws ParamètreInvalide;
}
