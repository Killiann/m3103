public class Banque {

    private static final int MAX_COMPTES = 100;

    private CompteBancaire[] compteBancaires;
    private int nbc;

    public Banque() {
        this.compteBancaires = new CompteBancaire[MAX_COMPTES];
    }

    public void ajouterCompte(CompteBancaire compteBancaire) throws ParamètreInvalide, TableauPlein {
        if(exist(compteBancaire.getLibellé()) != null) {
            throw new ParamètreInvalide("Compte Bancaire", "Déjà dans la liste des comptes de la banque");
        }

        if(this.nbc == MAX_COMPTES) {
            throw new TableauPlein();
        }

        this.compteBancaires[this.nbc] = compteBancaire;
        this.nbc++;
    }

    public void retirerCompte(CompteBancaire compteBancaire) throws ParamètreInvalide {
        Couple<CompteBancaire, Integer> couple = this.exist(compteBancaire.getLibellé());
        if(couple == null) {
            throw new ParamètreInvalide("Compte Bancaire", "Pas présent dans la liste des comptes de la banque");
        }

        this.compteBancaires[couple.getSeconds()] = null;
    }

    private Couple<CompteBancaire, Integer> exist(String libelle) {
        for (int i = 0; i < this.compteBancaires.length; i++) {
            CompteBancaire compteBancaire = this.compteBancaires[i];
            if(compteBancaire != null && compteBancaire.getLibellé().equals(libelle)) return new Couple<>(compteBancaire, i);
        }
        return null;
    }

    public void Virer(float montant, CompteBancaire sender, CompteBancaire receiver) throws ParamètreInvalide {
        if(montant <= 0) {
            throw new ParamètreInvalide("Montant", "Inférieur à 0");
        }

        if(!(sender instanceof Debitable)) {
            throw new ParamètreInvalide("Compter Bancaire envoyeur", "Il doit être débitable");
        }

        Couple<Devise, Float> couple = new Couple<>(sender.getDevise(), montant);
        ((Debitable)sender).debiter(couple);
        receiver.créditer(couple);
    }
}
