public class Couple<T, V> {

    private T first;
    private V seconds;

    public Couple(T first, V seconds) {
        this.first = first;
        this.seconds = seconds;
    }

    public T getFirst() {
        return first;
    }

    public V getSeconds() {
        return seconds;
    }
}
