public class CompteCourant extends CompteBancaire implements Debitable {

    public CompteCourant(String l, Devise d) {
        super(l, d);
    }

    @Override
    public void debiter(Couple<Devise, Float> couple) throws ParamètreInvalide {
        if(couple.getSeconds() <= 0) {
            throw new ParamètreInvalide("Montant", "Inférieur à 0");
        }
        couple = OutilsFinanciers.convertir(couple, this.getDevise());
        this.setSolde(this.getSolde() - couple.getSeconds());
    }
}
