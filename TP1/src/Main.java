public class Main {

    public static void main(String[] args) throws Exception {
        CompteCourant courant = new CompteCourant("1234", Devise.EURO);

        courant.créditer(new Couple<>(Devise.DOLLAR, 50F));
        System.out.println(courant);
        courant.debiter(new Couple<>(Devise.EURO, 4F));
        System.out.println(courant);

        CompteCourant courantt = new CompteCourant("3333", Devise.DOLLAR);
        System.out.println(courantt);
        new Banque().Virer(3F, courant, courantt);
        System.out.println(courantt);
        System.out.println(courant);
    }
}
