public abstract class CompteBancaire implements Creditable {

    private String libellé;
    private float solde;
    private Devise devise;

    public CompteBancaire(String l, Devise d) {
        this.libellé = l;
        this.devise = d;
    }

    public String getLibellé() {
        return libellé;
    }

    public float getSolde() {
        return solde;
    }

    public Devise getDevise() {
        return devise;
    }

    protected void setSolde(float solde) {
        this.solde = solde;
    }

    @Override
    public void créditer(Couple<Devise, Float> couple) throws ParamètreInvalide {
        if(couple.getSeconds() <= 0) {
            throw new ParamètreInvalide("Montant", "Inférieur à 0");
        }
        couple = OutilsFinanciers.convertir(couple, devise);
        this.setSolde(this.getSolde() + couple.getSeconds());
    }

    @Override
    public String toString() {
        return "Compte Bancaire : "+ this.libellé +" ("+ this.solde +" - "+ this.devise.name() +")";
    }
}
