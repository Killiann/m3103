public class Applications {

    public static void main(String[] args) {
        GestionnaireApplication gestionnaire = new GestionnaireApplication();

        Application desinstallApp = new App_Graphique();

        try {
            gestionnaire.ajouterApplication(new App_Système());
            gestionnaire.ajouterApplication(desinstallApp);
            gestionnaire.ajouterApplication(new App_Noyau());
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            gestionnaire.uninstall((Desinstalable) desinstallApp);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
