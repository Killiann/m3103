public interface Desinstalable {

    void uninstall() throws Exception;
}
