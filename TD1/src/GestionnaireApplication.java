public class GestionnaireApplication {

    private static final int MAX_NBA = 10000;

    private final Application[] applications;
    private int nba;

    public GestionnaireApplication() {
        this.applications = new Application[MAX_NBA];
        this.nba = 0;
    }

    public void ajouterApplication(Application application) throws Exception {
        if(this.nba == MAX_NBA) {
            throw new Exception("Tableau plein");
        }
        applications[this.nba++] = application;
    }

    public void uninstall(Desinstalable application) throws Exception {
        int index = -1;
        for (int i = 0; i < applications.length; i++) {
            if(applications[i] != null && applications[i].equals(application)) {
                index = i;
                break;
            }
        }

        if(index != -1) {
            application.uninstall();
            this.nba--;
            for (int j = index; j < this.nba; j++) {
                this.applications[j] = this.applications[j+1];
            }
        }
    }
}
