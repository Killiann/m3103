public class Carte implements Comparable<Carte> {

    private Valeur valeur;
    private Couleur couleur;

    public Carte(Valeur valeur, Couleur couleur) {
        this.valeur = valeur;
        this.couleur = couleur;
    }

    public Couleur getCouleur() {
        return couleur;
    }

    public Valeur getValeur() {
        return valeur;
    }

    @Override
    public String toString() {
        return this.valeur + " de " + this.couleur;
    }

    @Override
    public int compareTo(Carte c) {
        int v1 = this.valeur.ordinal();
        int c1 = this.couleur.ordinal();
        int v2 = c.valeur.ordinal();
        int c2 = c.couleur.ordinal();


        if(c1 < c2) {
            return -1;
        } else if(c1 > c2) {
            return 1;
        } else
            return v1-v2;

        //return this.equals(c) ? 0 : (c2 < c1 || c2==c1 && v2 < v1 ? 1 : -1);
    }
}
