import java.util.*;

public class JeuDeCarte {

    private static final Random RANDOM = new Random();

    private List<Carte> paquet;

    public JeuDeCarte() {
        this.paquet = new ArrayList<>();

        for (Couleur c : Couleur.values()) {
            for (Valeur v : Valeur.values()) {
                this.paquet.add(new Carte(v, c));
            }
        }
    }

    public Carte premiereCarte() throws PaquetVideException {
        if(this.paquet.isEmpty()) {
            throw new PaquetVideException();
        }

        return this.paquet.get(0);
    }

    public void classer() {
        Collections.sort(this.paquet);
    }

    public void melanger() {
        Collections.shuffle(this.paquet);
    }

    public String retourner() throws PaquetVideException {
        Carte carte = this.premiereCarte();

        Collections.rotate(this.paquet, -1);
        return carte.toString();
    }

    public void distribuer() throws PaquetVideException {
        if(this.paquet.isEmpty()) {
            throw new PaquetVideException();
        }

        this.paquet.remove(0);
    }
}
