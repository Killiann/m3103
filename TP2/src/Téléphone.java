import java.util.*;

public class Téléphone {

    private final String[] caractères = { "0 ", "1", "2abc", "3def", "4ghi",
            "5jkl", "6mno", "7pqrs", "8tuv", "9wxyz", "#,;:", "*?." };

    private final Touche[] touches;

    public Téléphone() {
        touches = new Touche[caractères.length];

        for (int i = 0; i < caractères.length; i++) {
            touches[i] = new Touche(caractères[i]);
        }
    }

    public String decode(String saisie) throws ListeVide {
        StringBuilder buffer = new StringBuilder();

        String[] str = saisie.split("\\.");
        for (String s : str) {

            ToucheElement last = null;
            List<ToucheElement> toucheElements = new ArrayList<>();
            for (char c : s.toCharArray()) {
                int cInt = charToInt(c);
                ToucheElement element = this.getElement(toucheElements, cInt);
                if(element == null) {
                    toucheElements.add(new ToucheElement(cInt, 1));
                    if(last != null) last.setBlocked(true);
                } else
                    element.up();

                last = element;
            }

            for (ToucheElement toucheElement : toucheElements) {
                buffer.append(this.touches[toucheElement.getC()].getCaractère(toucheElement.getNombre()));
            }
        }

        return buffer.toString();
    }

    public StringBuffer encode(String saisie) throws ListeVide {
        StringBuffer buffer = new StringBuffer();

        Touche lastTouche = null;
        for (char c : saisie.toCharArray()) {
            Touche toucheSelected = null;
            for (Touche touche : this.touches) {
                if(touche.getNb(c) != -1) {
                    toucheSelected = touche;
                    break;
                }
            }

            if(lastTouche != null && lastTouche.equals(toucheSelected)) {
                buffer.append(".");
            }

            for (int i = 0; i < toucheSelected.getNb(c); i++) {
                buffer.append(toucheSelected.getId());
            }

            lastTouche = toucheSelected;
        }
        return buffer;
    }

    private ToucheElement getElement(List<ToucheElement> list, int cInt) {
        for (ToucheElement toucheElement : list) {
            if(toucheElement.getC() == cInt && !toucheElement.isBlocked()) return toucheElement;
        }
        return null;
    }

    private int charToInt(char c) {
        return Integer.parseInt(String.valueOf(c == '#' ? "10" : (c == '*' ? "11" : c)));
    }

    private class ToucheElement {
        private final int c;
        private int nombre;
        private boolean blocked;

        public ToucheElement(int c, int nombre) {
            this.c = c;
            this.nombre = nombre;
        }

        public int getC() {
            return this.c;
        }

        public int getNombre() {
            return this.nombre;
        }

        public void up() {
            this.nombre++;
        }

        public void setBlocked(boolean blocked) {
            this.blocked = blocked;
        }

        public boolean isBlocked() {
            return this.blocked;
        }
    }
}
