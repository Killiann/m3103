public class Main {

    public static void main(String[] args) {
        Téléphone téléphone = new Téléphone();

        try {
            System.out.println(téléphone.decode("6622999"));
            System.out.println(téléphone.decode("6622999.9999"));
            System.out.println(téléphone.decode("44445555003333224444880022233322888"));
            System.out.println(téléphone.decode("###**"));
            System.out.println(téléphone.encode("max"));
            System.out.println(téléphone.encode("maxy"));
            System.out.println(téléphone.encode("il fait beau"));
        } catch (ListeVide listeVide) {
            listeVide.printStackTrace();
        }
    }
}
