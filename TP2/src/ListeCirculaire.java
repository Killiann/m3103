import java.util.ArrayList;
import java.util.List;

public class ListeCirculaire<T> {

    private List<T> tab;

    private int cursor;

    public ListeCirculaire() {
        this.tab = new ArrayList<>();
    }

    public void ajouter(T generic) {
        this.tab.add(generic);
    }

    public T premier() throws ListeVide {
        if(this.tab.isEmpty()) throw new ListeVide();
        this.cursor = 0;
        return this.tab.get(this.cursor);
    }

    public T suivant() throws ListeVide {
        if(this.tab.isEmpty()) throw new ListeVide();
        this.cursor++;
        if(this.cursor >= this.tab.size()) {
            this.cursor = 0;
        }
        return this.tab.get(this.cursor);
    }

    public List<T> getTab() {
        return tab;
    }
}

/*
VERSION AVEC UN TABLEAU

public class ListeCirculaire<T> {

    private static final int SIZE = 100;

    private T[] tab;

    private int nb;
    private int cursor;

    public ListeCirculaire() {
        this.tab = (T[]) new Object[SIZE];
    }

    public void ajouter(T generic) {
        if(this.nb == SIZE) {
            return;
        }

        this.tab[this.nb] = generic;
        this.nb++;
    }

    public T premier() throws ListeVide {
        if(nb == 0) throw new ListeVide();
        this.cursor = 0;
        return this.tab[this.cursor];
    }

    public T suivant() throws ListeVide {
        if(nb == 0) throw new ListeVide();
        this.cursor++;
        if(this.cursor >= this.nb) {
            this.cursor = 0;
        }
        return this.tab[this.cursor];
    }

    public T[] getTab() {
        return tab;
    }
}

 */