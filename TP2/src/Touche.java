public class Touche {

    private String name;
    private ListeCirculaire<Character> listeCirculaire;

    public Touche(String name) {
        this.name = name;
        this.listeCirculaire = new ListeCirculaire<>();
        for (char c : name.toCharArray()) {
            this.listeCirculaire.ajouter(c);
        }
    }

    public char getCaractère(int nb) throws ListeVide {
        char c = this.listeCirculaire.premier();
        for (int i = 0; i < nb-1; i++) {
            c = this.listeCirculaire.suivant();
        }
        return c;
    }

    public char getId() {
        return name.toCharArray()[0];
    }

    public int getNb(char c) throws ListeVide {
        int i = 1;
        for (Character character : listeCirculaire.getTab()) {
            if(character == c) return i;
            i++;
        }
        return -1;
    }
}
