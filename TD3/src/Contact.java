public class Contact implements Comparable<Contact> {

    private String nom;
    private String prenom;
    private String telephone;

    public Contact(String nom, String prenom, String telephone) throws ContactInvalid {
        String erreur = "";
        if(nom == null) {
            erreur+="Nom";
        }
        if(prenom == null) {
            erreur+=((!erreur.isEmpty() ? "," : "") + "prenom");
        }
        if(telephone == null) {
            erreur+=((!erreur.isEmpty() ? "," : "") + "telephone");
        }

        if(erreur.length() != 0) {
            throw new ContactInvalid(erreur);
        }

        this.nom = nom;
        this.prenom = prenom;
        this.telephone = telephone;
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getTelephone() {
        return telephone;
    }

    @Override
    public String toString() {
        return this.prenom + " " + this.nom + " " + this.telephone;
    }

    @Override
    public int compareTo(Contact o) {
        return this.toString().compareTo(o.toString());
    }
}
