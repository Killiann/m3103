import java.util.*;

public class Gestionnaire {

    private Map<String, Contact> contacts;

    public Gestionnaire() {
        this.contacts = new TreeMap<>();
    }

    public void ajouterContact(Contact contact) {
        if (contact != null) {
            this.contacts.put(contact.getPrenom() + " " + contact.getNom(), contact);
        }
    }

    public String versChaineOrdreAlpha1() {
        String s = "";
        for (String cle : this.contacts.keySet()) {
            s += this.contacts.get(cle).toString() + "\n";
        }
        return s;
    }

    public String versChaineOrdreAlpha2() {
        String s = "";
        List<String> cles = new ArrayList<>(this.contacts.keySet();
        Collections.sort(cles);
        for (String cle : cles) {
            s += this.contacts.get(cle).toString() + "\n";
        }
        return s;
    }

    public String versChaineOrdreAlpha3() {
        String s = "";
        for (Contact c : this.contacts.values()) {
            s += c.toString() + "\n";
        }
        return s;
    }

    public String versChaineOrdreAlpha() {
        String s = "";
        List<Contact> valeurs = new ArrayList<>(this.contacts.values();
        Collections.sort(valeurs);
        for (Contact c : valeurs) {
            s += c.toString() + "\n";
        }
        return s;
    }

    public Contact rechercherContact(String nom, String prenom) {
        return this.contacts.getOrDefault(prenom + " " + nom, null);
    }

}
