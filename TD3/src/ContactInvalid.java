public class ContactInvalid extends Exception {

    public ContactInvalid(String message) {
        super(message + " invalide");
    }
}
